using System.Collections.Generic;
using UnityEngine;

namespace GameSavvy.OUPoolSystem
{
    /// <summary>
    /// A pool that does not dynamically grow, it recycles objects in use
    /// Perfect for Decals and similar objects
    /// </summary>
    public class FixedSizePool : AObjectPool
    {
        private Queue<PoolableObject> _objectsInPool;
        private LinkedList<PoolableObject> _objectsInUse;

        /// <summary>
        /// A pool that does not dynamically grow, it recycles objects in use
        /// Perfect for Decals and similar objects
        /// </summary>
        /// <param name="prefab">the prefab object to create a pool for</param>
        /// <param name="poolSize">the maximum size of the pool</param>
        /// <param name="parent">spawned objects will be parented to this transform, for Editor Only</param>
        public FixedSizePool(PoolableObject prefab, int poolSize, Transform parent = null)
        {
            if (prefab == null)
            {
                Debug.LogError("Cannot create pool for NULL object, Prefab must not be NULL");
                return;
            }

            if (poolSize < 0) poolSize = PoolManager.DEFAULT_POOL_SIZE;

            _originalPrefab = prefab;
            PoolId = _originalPrefab.GetInstanceID();
            _originalPrefab.gameObject.SetActive(false);
            _parentObject = parent;
            PopulatePool(poolSize);
        }


        override
        protected void PopulatePool(int poolSize)
        {
            _objectsInUse = new LinkedList<PoolableObject>();
            _objectsInPool = new Queue<PoolableObject>(poolSize);
            for (int i = 0; i < poolSize; i++)
            {
                AddObjectToPool();
            }
        }

        override
        protected void AddObjectToPool()
        {
#if UNITY_EDITOR
            PoolableObject clone = GameObject.Instantiate(_originalPrefab, _parentObject);
#else
            PoolableObject clone = GameObject.Instantiate(_originalPrefab);
#endif
            clone.gameObject.SetActive(false);
            clone.SetPoolId(PoolId);
            _objectsInPool.Enqueue(clone);
        }


        override
        public void Release(PoolableObject clone)
        {
            if (clone == null)
            {
                Debug.LogWarning($"Trying to add NULL to existing pool [{PoolId}]");
                return;
            }

            _objectsInUse.Remove(clone);
            _objectsInPool.Enqueue(clone);
            clone.OnReleased();
        }

        override
        public T GetNext<T>()
        {
            if (_objectsInPool.Count == 0)
            {
                var recycled = _objectsInUse.First.Value;
                recycled.OnRecycled();
                recycled.gameObject.SetActive(false);
                _objectsInUse.Remove(recycled);
                _objectsInUse.AddLast(recycled);
                return recycled as T;
            }

            var obj = _objectsInPool.Dequeue();
            _objectsInUse.AddLast(obj);
            return obj as T;
        }
    }

}
