using UnityEngine;

namespace GameSavvy.OUPoolSystem
{
    [RequireComponent(typeof(PoolableObject))]
    public class PoolableLifespan : MonoBehaviour
    {
        [SerializeField]
        private float _lifeSpan = 3f;

        private PoolableObject _poolableObject;
        private float _startTime = 0;

        private void Awake()
        {
            _poolableObject = GetComponent<PoolableObject>();
        }

        private void OnEnable()
        {
            _startTime = Time.time;
        }

        private void Update()
        {
            if (Time.time >= _startTime + _lifeSpan)
            {
                _poolableObject.Release();
            }
        }
    }
}
