﻿using System.Collections.Generic;
using UnityEngine;

namespace GameSavvy.OUPoolSystem
{

    /// <summary>
    /// Static class that serves as the control center for Objects Pools
    /// You can control many types of pools for many objects
    /// </summary>
    public static class PoolManager
    {
        public static readonly int DEFAULT_POOL_SIZE = 5;
        private static Dictionary<int, AObjectPool> _objectPools = new Dictionary<int, AObjectPool>();

        /// <summary>
        /// Used to register a specific type of pool per Object/Prefab
        /// </summary>
        /// <param name="pool">The type of Pool to be used, Growable, FixedSize, etc...</param>
        public static void RegisterPool(AObjectPool pool)
        {
            if (pool == null)
            {
                Debug.LogError("Pool must not be NULL");
                return;
            }

            _objectPools[pool.PoolId] = pool;
        }

        /// <summary>
        /// Gets the next object from the pool, if no pool is created it automatically creates a growable pool for it
        /// </summary>
        public static T GetNext<T>(T prefab, Vector3 pos, Quaternion rot, Transform parent = null, bool setActive = true) where T : PoolableObject
        {
            if (prefab == null)
            {
                Debug.LogError($"Prefab [{prefab.gameObject.name}] must not be NULL");
                return null;
            }

            var obj = GetNext(prefab);
            obj.transform.position = pos;
            obj.transform.rotation = rot;
            if (parent != null)
            {
                obj.transform.SetParent(parent);
            }
            obj.gameObject.SetActive(setActive);
            return obj;
        }

        /// <summary>
        /// Gets the next object from the pool, if no pool is created it automatically creates a growable pool for it
        /// </summary>
        public static T GetNext<T>(T prefab) where T : PoolableObject
        {
            if (prefab == null)
            {
                Debug.LogError($"Cannot GetNext from NULL, Prefab Must not be NULL");
                return null;
            }

            int poolId = prefab.GetInstanceID();

            if (_objectPools.ContainsKey(poolId) == false)
            {
                Debug.LogWarning($"Pool ID [{poolId}] does not exist\nCreating a growable pool for it");
                _objectPools[poolId] = new GrowablePool(prefab, DEFAULT_POOL_SIZE);
            }

            return _objectPools[poolId].GetNext<T>();
        }

        /// <summary>
        /// Used to move used objects back into the pool, recycle objects
        /// </summary>
        public static void Release(this PoolableObject clone)
        {
            if (clone == null)
            {
                Debug.LogError($"Cannot add NULL back to pool, Clone Must not be NULL");
                return;
            }

            if (_objectPools.ContainsKey(clone.PoolId) == false)
            {
                Debug.LogError($"Cannot add back to pool, Pool ID [{clone.PoolId}] does not exist");
                return;
            }

            clone.gameObject.SetActive(false);
            _objectPools[clone.PoolId].Release(clone);
        }

    }//Class

}// Namespace
