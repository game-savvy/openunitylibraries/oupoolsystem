using System.Collections.Generic;
using UnityEngine;

namespace GameSavvy.OUPoolSystem
{

    /// <summary>
    /// A pool that uses a Stack and dynamically grows the size of the pool as needed
    /// </summary>
    public class GrowablePool : AObjectPool
    {

        private Stack<PoolableObject> _objectsInPool;

        /// <summary>
        /// A pool that uses a Stack and dynamically grows the size of the pool as needed
        /// </summary>
        /// <param name="prefab">the prefab object to create a pool for</param>
        /// <param name="poolSize">the initial pool size</param>
        /// <param name="parent">spawned objects will be parented to this transform, for Editor Only</param>
        public GrowablePool(PoolableObject prefab, int poolSize, Transform parent = null)
        {
            if (prefab == null)
            {
                Debug.LogError("Cannot create pool for NULL object, Prefab must not be NULL");
                return;
            }

            if (poolSize < 0) poolSize = PoolManager.DEFAULT_POOL_SIZE;

            _originalPrefab = prefab;
            PoolId = _originalPrefab.GetInstanceID();
            _originalPrefab.gameObject.SetActive(false);
            _parentObject = parent;
            PopulatePool(poolSize);
        }

        override
        protected void PopulatePool(int poolSize)
        {
            _objectsInPool = new Stack<PoolableObject>(poolSize);
            for (int i = 0; i < poolSize; i++)
            {
                AddObjectToPool();
            }
        }

        override
        protected void AddObjectToPool()
        {
#if UNITY_EDITOR
            PoolableObject clone = GameObject.Instantiate(_originalPrefab, _parentObject);
#else
            PoolableObject clone = GameObject.Instantiate(_originalPrefab);
#endif
            clone.gameObject.SetActive(false);
            clone.SetPoolId(PoolId);
            _objectsInPool.Push(clone);
        }

        override
        public T GetNext<T>()
        {
            if (_objectsInPool.Count == 0)
            {
                AddObjectToPool();
            }

            return _objectsInPool.Pop() as T;
        }

        override
        public void Release(PoolableObject clone)
        {
            if (clone == null)
            {
                Debug.LogWarning($"Trying to add NULL to existing pool [{PoolId}]");
                return;
            }

            _objectsInPool.Push(clone);
            clone.OnReleased();
        }
    }

}
