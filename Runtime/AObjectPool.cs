using UnityEngine;

namespace GameSavvy.OUPoolSystem
{
    /// <summary>
    /// Abstract class that serves as a base for specific types of Object Pools
    /// </summary>
    public abstract class AObjectPool
    {
        /// <summary>
        /// The Unique InstanceID of the prefab
        /// </summary>
        /// <value></value>
        public int PoolId { get; protected set; }

        protected PoolableObject _originalPrefab;
        protected Transform _parentObject;

        abstract public void Release(PoolableObject clone);
        abstract public T GetNext<T>() where T : PoolableObject;

        abstract protected void PopulatePool(int poolSize);
        abstract protected void AddObjectToPool();
    }
}
