using UnityEngine;

namespace GameSavvy.OUPoolSystem
{
    public class PoolableObjectSpawner : MonoBehaviour
    {
        [SerializeField]
        private PoolableObject _prefab;

        public void Spawn()
        {
            PoolManager.GetNext(_prefab, transform.position, transform.rotation);
        }

        public void Spawn(Transform targetLocation)
        {
            PoolManager.GetNext(_prefab, targetLocation.position, targetLocation.rotation);
        }
    }
}
