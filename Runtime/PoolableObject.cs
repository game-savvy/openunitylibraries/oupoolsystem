﻿using UnityEngine;
using UnityEngine.Events;

namespace GameSavvy.OUPoolSystem
{
    public class PoolableObject : MonoBehaviour
    {
        public int PoolId { get; protected set; } = -1;

        [SerializeField]
        protected UnityEvent<Transform> _onReleased;

        [SerializeField]
        protected UnityEvent<Transform> _onRecycled;

        public void SetPoolId(int poolId)
        {
            if (PoolId == -1)
            {
                PoolId = poolId;
            }
        }

        virtual
        public void OnReleased()
        {
            _onReleased.Invoke(transform);
        }

        virtual
        public void OnRecycled()
        {
            _onRecycled.Invoke(transform);
        }

    }//Class

}// Namespace
