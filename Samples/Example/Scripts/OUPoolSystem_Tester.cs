﻿using UnityEngine;
using Sirenix.OdinInspector;

namespace GameSavvy.OUPoolSystem.Samples
{
    public class OUPoolSystem_Tester : MonoBehaviour
    {

        [SerializeField]
        private PoolableObject _projectile1 = null;

        [SerializeField]
        private PoolableObject _projectile2 = null;

        [SerializeField]
        private PoolableProjectile _projectile3 = null;

        [SerializeField]
        private Transform _projectiles1Parent = null;

        [SerializeField]
        private Transform _projectiles2Parent = null;

        [SerializeField]
        private Transform _projectiles3Parent = null;


        private void Start()
        {
            PoolManager.RegisterPool(new FixedSizePool(_projectile1, 5, _projectiles1Parent));
            PoolManager.RegisterPool(new GrowablePool(_projectile2, 5, _projectiles2Parent));
            PoolManager.RegisterPool(new GrowablePool(_projectile3, 5, _projectiles3Parent));
        }

        [Button]
        private void SpawnProjectile1()
        {
            PoolManager.GetNext(_projectile1, Vector3.left, Quaternion.identity);
        }

        [Button]
        private void SpawnProjectile2()
        {
            PoolManager.GetNext(_projectile2, Vector3.right, Quaternion.identity);
        }

        [Button]
        private void SpawnProjectile3()
        {
            PoolManager.GetNext(_projectile3, Vector3.right, Quaternion.identity);
        }

    }

}
