﻿using UnityEngine;

namespace GameSavvy.OUPoolSystem.Samples
{

    [RequireComponent(typeof(Rigidbody))]
    [RequireComponent(typeof(PoolableObject))]
    public class OUPoolSystem_Sample_Projectile : MonoBehaviour
    {
        [SerializeField]
        private float _projectileSpeed = 30f;

        private Rigidbody _rigidBody;

        private void Awake()
        {
            _rigidBody = GetComponent<Rigidbody>();
        }

        private void OnEnable()
        {
            _rigidBody.velocity = transform.forward * _projectileSpeed;
            _rigidBody.angularVelocity = Vector3.zero;
        }

    }
}
