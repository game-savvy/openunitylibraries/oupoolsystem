## Want to Help? How to contribute to this Repo

For all code, please follow these [coding standards and naming conventions](https://github.com/wcampospro/CodingConvention_Unity).
Simply work on your fork, create a feature branch and when done please submit a pull request.