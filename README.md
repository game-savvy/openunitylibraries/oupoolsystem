# OUPoolSystem

Open Unity Pool System, is a ready to use, open source, extensible **Object Pooling** library
  
  
___
## How to Install

### **With the GitUrl**
1. First, open the **Package Manager Window**  
![Open Package Manager](https://gitlab.com/wcampospro/ouattributes/raw/61f1ccabebefa2c9ed8550455a42668dcdadf96d/Documentation~/Images/Unity_PackageManagerWindow.png)

2. Second, on the top left **+** sign, select Add Package from git URL...  
![PackageManager Add from Git URL](https://gitlab.com/wcampospro/ouattributes/raw/61f1ccabebefa2c9ed8550455a42668dcdadf96d/Documentation~/Images/PackageManager_GitURL.png)

3. Third, paste The ***HTTPS GitURL*** in the text field

The library is ready to be used...  
  
After this , you will be able to find the Package in the **Package Manager** window.
___
## System Requirements
Unity 2019.3.0f1 or later versions.

___
## Dependencies

This library depends on:  
- None
  
---
## Want to Help? How to contribute to this Repo

For all code, please follow these [coding standards and naming conventions][LinkUnityCodingStandards].  
Simply work on your fork, create a feature branch and when done please submit a pull request.
  
---
## License
MIT License

Copyright (c) 2020 Willy Campos

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.


[LinkUnityCodingStandards]: https://gitlab.com/wcampospro/CodingConvention_Unity
